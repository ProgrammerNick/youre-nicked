#!/bin/sh

if [ -z $1 ]
then  
  echo "I need a parameter to identify what to name the artifact resulting from this build!";
  exit 1;
fi

ARTIFACT_ID=$1
echo "Artifact ID: $ARTIFACT_ID"

touch public/manifest.txt
echo "Artifact ID: $ARTIFACT_ID" >> public/manifest.txt
echo "Commit: $(git rev-parse HEAD)" >> public/manifest.txt

DEST=s3://nick-sites/youre-nicked/$ARTIFACT_ID/
echo "Uploading public/* to $DEST. This should probably be zipped..."
aws s3 cp public $DEST --profile jenkins-nick --recursive
